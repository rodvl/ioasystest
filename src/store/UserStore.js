const AUTHENTICATE_USER = "user/AUTHENTICATE_USER";
const UNAUTHENTICATE_USER = "user/UNAUTHENTICATE_USER";

export const INITIAL_STATE = {
  id: null,
  name: "",
  email: "",
  birthdate: "",
  gender: "",
  isAuthenticated: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case AUTHENTICATE_USER: {
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
      };
    }
    case UNAUTHENTICATE_USER: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}

export const authenticateUser = (userData) => {
  return {
    type: AUTHENTICATE_USER,
    payload: userData,
  };
};

export const unauthenticateUser = () => {
  return {
    type: UNAUTHENTICATE_USER,
    payload: INITIAL_STATE,
  };
};

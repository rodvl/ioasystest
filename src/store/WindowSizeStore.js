const UPDATE_WINDOW_WIDTH = "window/UPDATE_WINDOW_WIDTH";

export const INITIAL_STATE = {
  windowDimensions: null,
  isMobile: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE_WINDOW_WIDTH: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
}

export const updateWindowInformations = (data) => {
  return {
    type: UPDATE_WINDOW_WIDTH,
    payload: data,
  };
};

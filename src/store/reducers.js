import { combineReducers } from "redux";
import UserStore from "./UserStore";
import WindowSizeStore from "./WindowSizeStore";

const reducers = combineReducers({
  user: UserStore,
  window: WindowSizeStore,
});

export default reducers;

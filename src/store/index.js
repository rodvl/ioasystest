import { createStore, compose } from "redux";
import reducers from "./reducers";

const devEnv = true;

const composeEnchancers =
  (devEnv ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null) || compose;

const store = createStore(reducers, composeEnchancers());

export default store;

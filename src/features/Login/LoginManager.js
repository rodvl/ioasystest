import bookApi from "../../services/bookApi";

class LoginManager {
  async authenticate(email, password) {
    const response = await bookApi.post("/auth/sign-in", { email, password });

    const userData = response && response.data;

    const token =
      response && response.headers && response.headers.authorization;

    return { userData, token };
  }
}

export default new LoginManager();

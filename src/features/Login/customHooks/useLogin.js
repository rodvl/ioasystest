import { useState } from "react";
import LoginManager from "../LoginManager";
import { storageBookApiToken } from "../../../shared/utils/Constants";

const useLogin = (authenticateUser) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [openTooltip, setOpenToolTipo] = useState(false);

  const handleClickAuthenticate = async () => {
    setOpenToolTipo(false);
    try {
      const data = await LoginManager.authenticate(email, password);
      localStorage.setItem(storageBookApiToken, data.token);
      authenticateUser(data.userData);
    } catch (e) {
      setOpenToolTipo(true);
    }
  };

  return {
    email,
    setEmail,
    password,
    setPassword,
    openTooltip,
    handleClickAuthenticate,
  };
};

export default useLogin;

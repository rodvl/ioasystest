import React from "react";
import { connect } from "react-redux";
import {
  Grid,
  Button,
  TextField,
  Tooltip,
  makeStyles,
  InputAdornment,
} from "@material-ui/core";
import * as UserStoreActions from "../../../store/UserStore";
import useLogin from "../customHooks/useLogin";
import LoginBackground from "../../../img/LoginBackground.png";
import WhiteLogo from "../../../img/WhiteLogo.png";

const Login = (props) => {
  const { authenticateUser, isMobile } = props;
  const classes = useStyles({ isMobile });

  const {
    email,
    setEmail,
    password,
    setPassword,
    openTooltip,
    handleClickAuthenticate,
  } = useLogin(authenticateUser);

  return (
    <div className={classes.content}>
      <Grid
        container
        className={classes.container}
        direction="column"
        justify="center"
      >
        <Grid container item xs={12} md={6} spacing={3}>
          <Grid item xs={12}>
            <img src={WhiteLogo} alt="logo" className={classes.logo} />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              variant="outlined"
              className={classes.textfield}
              InputProps={{
                className: classes.input,
              }}
              InputLabelProps={{
                className: classes.input,
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <Tooltip
              arrow
              placement="bottom-start"
              open={openTooltip}
              title="Email e/ou senha incorretos."
            >
              <TextField
                label="Senha"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                variant="outlined"
                type="password"
                className={classes.textfield}
                InputLabelProps={{
                  className: classes.input,
                }}
                InputProps={{
                  className: classes.input,
                  endAdornment: (
                    <InputAdornment position="end">
                      <Button
                        disabled={!email || !password}
                        onClick={() => handleClickAuthenticate()}
                        variant="outlined"
                        className={classes.button}
                      >
                        ENTRAR
                      </Button>
                    </InputAdornment>
                  ),
                }}
              />
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

const useStyles = makeStyles({
  button: {
    width: "85px",
    height: "36px",
    borderRadius: "44px",
    color: "#B22E6F",
    border: "none",
    backgroundColor: "#FFF",
    fontWeight: 600,
    fontSize: "normal",
  },
  content: {
    background: `url(${LoginBackground}) no-repeat center center fixed`,
    WebkitBackgroundSize: "cover",
    MozBackgroundSize: "cover",
    OBackgroundSize: "cover",
    backgroundSize: "cover",
    height: "100%",
    width: "100%",
    position: "fixed",
  },
  textfield: (props) => ({
    backgroundColor: "rgba(0, 0, 0, 0.32)",
    width: props.isMobile ? "100%" : "368px",
    "& .MuiOutlinedInput-root": {
      "&.Mui-focused fieldset": {
        borderColor: "#FFF",
      },
    },
  }),
  input: {
    color: "#fff",
    "&.MuiFormLabel-root.Mui-focused": {
      color: "#fff",
    },
  },
  container: (props) => ({
    paddingLeft: props.isMobile ? "16px" : "115px",
    minHeight: "100vh",
  }),
  logo: {
    height: "50px",
  },
});

const mapStateToProps = (state) => ({
  isMobile: state.window.isMobile,
});

const mapDispatchToProps = (dispatch) => ({
  authenticateUser: (data) => dispatch(UserStoreActions.authenticateUser(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

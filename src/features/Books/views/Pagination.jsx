import React from "react";
import { connect } from "react-redux";
import { IconButton, Grid, Typography, makeStyles } from "@material-ui/core";
import { ChevronLeft, ChevronRight } from "react-feather";

const Pagination = (props) => {
  const { setPage, page, totalPages, isMobile } = props;
  const classes = useStyles({ isMobile });

  const returnButton = () => (
    <IconButton
      disabled={page === 1}
      onClick={() => setPage(page - 1)}
      className={`${classes.button} ${classes.backButton}`}
    >
      <ChevronLeft size={20} />
    </IconButton>
  );
  const forwardButton = () => (
    <IconButton
      disabled={page === totalPages}
      onClick={() => setPage(page + 1)}
      className={classes.button}
    >
      <ChevronRight size={20} />
    </IconButton>
  );
  const pageText = () => (
    <Typography className={classes.typography}>
      Página <span className={classes.bold}>{page}</span> de{" "}
      <span className={classes.bold}>{totalPages}</span>
    </Typography>
  );
  return (
    <Grid item xs={12}>
      <div className={classes.position}>
        {isMobile ? (
          <>
            {returnButton()}
            {pageText()}
            {forwardButton()}{" "}
          </>
        ) : (
          <>
            {pageText()}
            {returnButton()}
            {forwardButton()}
          </>
        )}
      </div>
    </Grid>
  );
};

const useStyles = makeStyles({
  typography: {
    display: "inline",
    fontSize: "12px",
    marginRight: "12px",
  },
  bold: {
    fontWeight: 600,
  },
  button: {
    border: "1px solid rgba(51, 51, 51, 0.2)",
    padding: "4px",
  },
  backButton: {
    marginRight: "12px",
  },
  position: (props) => ({
    width: "176px",
    marginLeft: "auto",
    marginRight: props.isMobile ? "auto" : "",
  }),
});

const mapStateToProps = (state) => ({
  isMobile: state.window.isMobile,
});

export default connect(mapStateToProps)(Pagination);

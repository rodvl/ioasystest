import React from "react";
import { Paper, Typography, Grid, makeStyles } from "@material-ui/core";

const BookCard = (props) => {
  const { book, selectItem } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.paper} onClick={() => selectItem(book)}>
      <Grid container item xs={12} spacing={2}>
        <Grid item xs={5}>
          <div className={classes.image}>
            <img
              src={book.imageUrl}
              alt={book.title}
              className={classes.image}
            />
          </div>
        </Grid>
        <Grid item xs={7}>
          <Typography className={classes.title}>{book.title}</Typography>
          {book.authors.map((e, index) => (
            <Typography key={e} className={classes.author}>
              {e}
              {index === book.authors.length - 1 ? "" : ","}
            </Typography>
          ))}
          <Typography className={classes.details}>
            {book.pageCount} páginas
            <br />
            {book.publisher}
            <br />
            Publicado em {book.published}
          </Typography>
        </Grid>
      </Grid>
    </Paper>
  );
};

const useStyles = makeStyles({
  image: {
    width: "100px",
    height: "160px",
  },
  paper: {
    cursor: "pointer",
    minHeight: "160px",
    maxHeight: "160px",
    padding: "12px",
  },
  title: {
    fontSize: "14px",
    fontWeight: 600,
  },
  author: {
    fontSize: "12px",
    color: "#AB2680",
  },
  details: {
    fontSize: "12px",
    color: "#999999",
    paddingTop: "8px",
  },
});

export default BookCard;

import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import BookCard from "./BookCard";
import Pagination from "./Pagination";
import BookDetails from "./BookDetails";
import useBooks from "../customHooks/useBooks";

const Books = () => {
  const classes = useStyles();
  const {
    pageBooks,
    setSelectedItem,
    setPage,
    page,
    totalPages,
    openModal,
    selectedItem,
    handleCloseModal,
  } = useBooks();

  return (
    <Grid container className={classes.container}>
      <Grid container item xs={12} spacing={3}>
        {pageBooks.map((e) => (
          <Grid item xs={12} md={3} key={e.id}>
            <BookCard book={e} selectItem={setSelectedItem} />
          </Grid>
        ))}
      </Grid>
      <Grid
        container
        item
        xs={12}
        className={classes.pagination}
        alignContent="center"
      >
        <Pagination setPage={setPage} page={page} totalPages={totalPages} />
      </Grid>
      <BookDetails
        openModal={openModal}
        book={selectedItem}
        handleCloseModal={handleCloseModal}
      />
    </Grid>
  );
};

const useStyles = makeStyles({
  pagination: {
    paddingTop: "16px",
    paddingBottom: "16px",
    paddingRight: "12px",
  },
  container: {
    paddingLeft: "20px",
  },
});

export default Books;

import React from "react";
import { connect } from "react-redux";
import {
  Box,
  IconButton,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { X } from "react-feather";

const BookDetails = (props) => {
  const { openModal, book, handleCloseModal, isMobile } = props;
  const classes = useStyles({ isMobile });

  return (
    openModal && (
      <Box className={classes.box}>
        <div>
          <IconButton
            className={classes.closeButton}
            onClick={() => handleCloseModal()}
          >
            <X size={20} />
          </IconButton>
        </div>
        <Grid container className={classes.content} spacing={3}>
          <Grid item xs={12} md={5}>
            <div className={classes.image}>
              <img
                src={book.imageUrl}
                alt={book.title}
                className={classes.image}
              />
            </div>
          </Grid>
          <Grid container item xs={12} md={7} spacing={3}>
            <Grid item xs={12}>
              <Typography className={classes.title}>{book.title}</Typography>
              <Typography className={classes.authors}>
                {book.authors.map(
                  (e, index) =>
                    `${e} ${index === book.authors.length - 1 ? "" : ","}`
                )}
              </Typography>
            </Grid>
            <Grid container item xs={12}>
              <Grid item xs={12}>
                <Typography className={classes.infoName}>
                  INFORMAÇÕES
                </Typography>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>Páginas</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>
                    {book.pageCount} Páginas
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>Editora</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>
                    {book.publisher}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>
                    Publicação
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>
                    {book.published}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>Idioma</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>
                    {book.language}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>
                    Titulo Original
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>{book.title}</Typography>
                </Grid>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>ISBN-10</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>
                    {book.isbn10}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item xs={12}>
                <Grid item xs={6}>
                  <Typography className={classes.infoName}>ISBN-13</Typography>
                </Grid>
                <Grid item xs={6}>
                  <Typography className={classes.info}>
                    {book.isbn13}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12}>
              <Typography className={classes.infoName}>
                RESENHA DA EDITORA
              </Typography>
              <Typography className={classes.info}>
                {book.description}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    )
  );
};

const useStyles = makeStyles({
  box: {
    display: "block",
    position: "fixed",
    overflow: "auto",
    zIndex: 1,
    left: 0,
    top: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.4)",
  },
  content: {
    backgroundColor: "#fff",
    width: "70%",
    margin: "5% auto",
  },
  closeButton: {
    display: "flex",
    marginLeft: "auto",
    marginRight: "16px",
    marginTop: "8px",
    backgroundColor: "#fff",
  },
  image: (props) => ({
    width: props.isMobile ? "240px" : "360px",
    height: props.isMobile ? "360px" : "520px",
  }),
  title: {
    fontSize: "28px",
    fontWeight: 600,
  },
  authors: {
    fontSize: "12px",
    color: "#AB2680",
  },
  infoName: {
    fontSize: "12px",
    fontWeight: 600,
  },
  info: {
    fontSize: "12px",
    color: "#999999",
  },
});
const mapStateToProps = (state) => ({
  isMobile: state.window.isMobile,
});

export default connect(mapStateToProps)(BookDetails);

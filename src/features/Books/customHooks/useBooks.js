import { useEffect, useState } from "react";
import BooksManager from "../BooksManager";

const useBooks = () => {
  const [pageBooks, setPageBooks] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [selectedItem, setSelectedItem] = useState();
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    const getData = async () => {
      const pageElements = 12;
      const response = await BooksManager.getBooks(page, pageElements);
      if (response) {
        const totalPageDecimals = response.totalPages % 1;
        if (totalPageDecimals !== 0)
          response.totalPages += 1 - totalPageDecimals;
        setPageBooks(response.data);
        setTotalPages(response.totalPages);
      }
    };
    getData();
  }, [page]);

  useEffect(() => {
    if (selectedItem) setOpenModal(true);
  }, [selectedItem]);

  const handleCloseModal = () => {
    setOpenModal(false);
    setSelectedItem();
  };
  return {
    pageBooks,
    setSelectedItem,
    setPage,
    page,
    totalPages,
    openModal,
    selectedItem,
    handleCloseModal,
  };
};

export default useBooks;

import bookApi from "../../services/bookApi";

class BooksManager {
  async getBooks(page, amount) {
    const response = await bookApi.get("/books", { params: { page, amount } });

    return response && response.data;
  }
}

export default new BooksManager();

import React from "react";
import { connect } from "react-redux";
import { Grid, IconButton, makeStyles, Typography } from "@material-ui/core";
import { LogOut } from "react-feather";
import * as UserStoreActions from "../../../store/UserStore";
import BlackLogo from "../../../img/BlackLogo.png";
import LayoutBackground from "../../../img/LayoutBackground.png";
import AuxLayoutBackground from "../../../img/AuxLayoutBackground.png";

const Layout = (props) => {
  const { isMobile, user, unauthenticateUser, children } = props;
  const classes = useStyles({ isMobile });
  return (
    <div className={classes.background}>
      <Grid container className={classes.content}>
        <Grid container item xs={12} className={classes.header}>
          <Grid item xs={6}>
            <img src={BlackLogo} alt="logo" className={classes.logo} />
          </Grid>
          <Grid item xs={6} alignContent="flex-end">
            <div className={classes.positionLogout}>
              {!isMobile && (
                <Typography className={classes.welcomeTypography}>
                  Bem Vindo, <span className={classes.name}>{user.name}!</span>
                </Typography>
              )}
              <IconButton
                onClick={() => unauthenticateUser()}
                className={classes.button}
              >
                <LogOut size={20} />
              </IconButton>
            </div>
          </Grid>
        </Grid>
        <Grid container item xs={12}>
          {children}
        </Grid>
      </Grid>
    </div>
  );
};

const useStyles = makeStyles({
  content: (props) => ({
    paddingLeft: props.isMobile ? "16px" : "50px",
    paddingRight: props.isMobile ? "16px" : "50px",
  }),
  welcomeTypography: {
    display: "inline-flex",
    fontSize: "12px",
  },
  header: {
    height: "88px",
    paddingTop: "20px",
    paddingBottom: "20px",
    paddingRight: "24px",
    paddingLeft: "20px",
  },
  button: {
    marginLeft: "16px",
    padding: "8px",
    border: "1px solid rgba(51, 51, 51, 0.2)",
  },
  logo: (props) => ({
    height: props.isMobile ? "30px" : "50px",
  }),
  name: {
    fontWeight: 600,
  },
  background: {
    background: `url(${LayoutBackground}) no-repeat center center fixed, url(${AuxLayoutBackground}) no-repeat center center fixed`,
    WebkitBackgroundSize: "cover",
    MozBackgroundSize: "cover",
    OBackgroundSize: "cover",
    backgroundSize: "cover",
    height: "100%",
    width: "100%",
    position: "fixed",
    backgroundBlendMode: "darken",
    overflow: "auto",
  },
  positionLogout: {
    float: "right",
  },
});

const mapStateToProps = (state) => ({
  isMobile: state.window.isMobile,
  user: state.user,
});

const mapDispatchToProps = (dispatch) => {
  return {
    unauthenticateUser: (data) =>
      dispatch(UserStoreActions.unauthenticateUser(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);

import axios from "axios";
import configureEnvironment from "../config/env";
import { storageBookApiToken } from "../shared/utils/Constants";

const bookApiClient = axios.create({
  headers: {},
  baseURL: configureEnvironment().API_UTL,
});

bookApiClient.interceptors.request.use((config) => {
  const token = localStorage.getItem(storageBookApiToken);

  const newConfig = config;

  if (token) newConfig.headers.Authorization = `bearer ${token}`;

  return newConfig;
});

export default bookApiClient;

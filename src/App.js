/* eslint-disable react/jsx-filename-extension */
import React from "react";
import { connect } from "react-redux";
import Routes from "./Routes";
import useWindowDimensions from "./shared/utils/useWindowDimensions";
import * as windowSizeStoreActions from "./store/WindowSizeStore";

function App(props) {
  const { userData, updateWidth } = props;
  const { windowDimensions, isMobile } = useWindowDimensions();
  updateWidth({ windowDimensions, isMobile });
  return <Routes userData={userData} />;
}

const mapStateToProps = (state) => ({
  userData: state.user,
});

const mapDispatchToProps = (dispatch) => {
  return {
    updateWidth: (data) =>
      dispatch(windowSizeStoreActions.updateWindowInformations(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

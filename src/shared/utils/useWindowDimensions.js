import { useState, useEffect } from "react";
import { mobileMaxSize } from "./Constants";

const getWindowDimensions = () => {
  const { innerWidth: width } = window;

  return width;
};

const verifyIsMobile = () => {
  return getWindowDimensions() <= mobileMaxSize;
};

export default function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );
  const [isMobile, setIsMobile] = useState(verifyIsMobile());

  useEffect(() => {
    const handleResize = () => {
      setWindowDimensions(getWindowDimensions());
      setIsMobile(verifyIsMobile());
    };

    window.addEventListener("resize", handleResize);
  }, []);
  return { windowDimensions, isMobile };
}

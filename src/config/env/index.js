import develop from "./develop";

function configureEnvironment() {
  const hostName = window && window.location && window.location.hostname;

  if (hostName.startsWith("OTHER_ENV_URL")) {
    return {};
  }
  return develop;
}

export default configureEnvironment;

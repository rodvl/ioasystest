const envConfig = {
  PUBLIC_URL: "http://localhost:3000",
  API_UTL: "https://books.ioasys.com.br/api/v1",
};

export default envConfig;

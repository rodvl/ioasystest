import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import Layout from "./features/layout";
import Login from "./features/Login";
import Books from "./features/Books";

const Routes = (props) => {
  const { userData } = props;
  return userData && userData.isAuthenticated ? (
    <Switch>
      <Layout>
        <Route exact path="/books" component={Books} />
        <Redirect to="/books" />
      </Layout>
    </Switch>
  ) : (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Redirect to="/login" />
    </Switch>
  );
};

export default Routes;
